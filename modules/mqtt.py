import paho.mqtt.client as mqtt
from modules.config import Config
from threading import Thread
import time
import ssl

WILL = '{"dead": true}'
DEF_QOS = 0
CA_PATH = "/telemetry-analytics/certs/ca.crt"
CERT_PATH = "/telemetry-analytics/certs/telemetry.crt"
KEY_PATH = "/telemetry-analytics/certs/telemetry.key"
ALPN_PROTOCOLS = "x-amzn-mqtt-ca"

config = Config()


def on_connect(client, userdata, flags, ret):
    if ret == 0:
        config.logger.info(f"MQTT Client connected to Broker at {config.mqtt_broker}:{config.mqtt_port}")
    else:
        config.logger.error(f"MQTT Connect Error st: {ret} flags: {flags}")


def on_disconnect(client, userdata, rc):
    config.logger.error(
        "MQTT Broker connection lost. Trying to reconnect in background..."
    )


class MqttClient:
    def __init__(self):

        # Init Thread
        self.mqtt_thr = Thread(target=self.mqtt_thr_cbk, daemon=True)

        # Set internal variables
        device_id = config.id
        self.telemetry_topic = f"jjsmarthome/v1/devices/{device_id}/telemetry"
        self.endpoint = (config.mqtt_broker, config.mqtt_port)

        # Init MQTT client
        self.mqtt_cli = mqtt.Client(device_id)
        self.mqtt_cli.on_connect = on_connect
        self.mqtt_cli.on_disconnect = on_disconnect
        self.mqtt_cli.username_pw_set(device_id, password="")
        self.mqtt_cli.will_set(self.telemetry_topic, WILL, 1)
        self.mqtt_cli.user_data_set(self)
        self.connection_failed = False

        if config.mqtt_protocol == "tls":
            config.logger.info("TLS authentication enabled")
            try:
                ssl_context = ssl.create_default_context()
                #ssl_context.set_alpn_protocols([ALPN_PROTOCOLS])
                ssl_context.load_verify_locations(cafile=CA_PATH)
                ssl_context.load_cert_chain(certfile=CERT_PATH, keyfile=KEY_PATH)
                self.mqtt_cli.tls_set_context(context=ssl_context)
            except Exception as e:
                config.logger.error("Exception when setting SSL context")
                raise e

    def start(self):
        self.mqtt_thr.start()
        while not self.mqtt_cli.is_connected():
            config.logger.info("Waiting for connection to MQTT Broker ...")
            if self.connection_failed:
                config.logger.error(
                    "Connection to MQTT Broker {}:{} failed".format(
                        self.endpoint[0], self.endpoint[1]
                    )
                )
                exit(1)
            time.sleep(1)

    def stop(self):
        self.mqtt_cli.disconnect()

    def send(self, data, topic):
        ret = self.mqtt_cli.publish(topic, data, DEF_QOS)
        if ret.rc == mqtt.MQTT_ERR_SUCCESS:
            config.logger.debug(f"Msg {data} sent to topic {topic}")
            return True

        config.logger.debug(f"Msg {data} could not be sent to topic {topic}")
        return False

    def mqtt_thr_cbk(self):
        try:
            self.mqtt_cli.connect(self.endpoint[0], self.endpoint[1], 60)
        except BaseException:
            self.connection_failed = True
            exit(1)
        self.mqtt_cli.loop_forever(retry_first_connection=True)
